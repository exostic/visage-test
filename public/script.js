const $location = document.getElementById('location');
const $results = document.getElementById('results');
const $submit = document.getElementById('submit');
const $modal = document.getElementById('modal');
const $unsaveLinks = document.getElementsByClassName('unsave-link');
const $saveLinks = document.getElementsByClassName('save-link');

let savedJobs = [];
let lastResult = [];

function showResults() {

    $results.innerHTML = '<div id="loading">...</div>';

    fetch('/search/' + $location.value)
    .then(response => response.json())
    .then(jobs => {

        jobs.sort((a,b) => {

            return (new Date(b.created_at) - new Date(a.created_at));
        });

        lastResult = jobs;
        updateResults(jobs);
    });
}

function updateResults(jobs) {

    $results.innerHTML = formatResults(jobs);

    for (let $saveLink of $saveLinks) {

        let savedJobIds = savedJobs.map(job => job.id);

        $saveLink.addEventListener('click', e => {

            let filtredJobs = jobs.filter(job => {

                return job.id === $saveLink.id;
            });

            savedJobs.push(filtredJobs[0]);
            updateSavedJobs(savedJobs);
            updateResults(lastResult);
        });
    }
}

function updateSavedJobs(jobs) {

    $modal.innerHTML = formatSavedJobs(jobs);

    for (let $unsaveLink of $unsaveLinks) {

        $unsaveLink.addEventListener('click', e => {

            let filtredJobs = jobs.filter(job => {

                return job.id !== $unsaveLink.id;
            });

            savedJobs = filtredJobs;
            updateSavedJobs(savedJobs);
            updateResults(lastResult);
        });
    }
}

function formatSavedJobs(jobs) {

    let html = '';

    if (jobs.length === 0) {

        html = 'no saved jobs';

    } else {

        html += '<li>';

        for(let job of jobs) {

            html += '<ul>';
            html +=     '<div class="job-container">';
            html +=         '<div class="job-left">';
            html +=             '<a href="https://jobs.github.com/positions/' + job.id + '" target="_blank">';
            html +=                 job.title;
            html +=             '</a>';
            html +=             '<br />';
            html +=             '<em>';
            html +=                 job.location;
            html +=             '</em>';
            html +=         '</div>';
            html +=         '<div class="job-right">';
            html +=             '<a href="#" class="unsave-link" id="' + job.id + '">';
            html +=                 'unsave';
            html +=             '</a>';
            html +=         '</div>';
            html +=     '</div>';
            html += '</ul>';
        }

        html += '</li>';
    }

    return html;
}

function formatResults(jobs) {

    let html = '';

    if (jobs.length === 0) {

        html = 'no result to show';

    } else {

        html += '<li>';

        for(let job of jobs) {

            html += '<ul>';
            html +=     '<div class="job-container">';
            html +=         '<div class="job-left">';
            html +=             '<a href="https://jobs.github.com/positions/' + job.id + '" target="_blank">';
            html +=                 job.title;
            html +=             '</a>';
            html +=             '<br />';
            html +=             job.created_at;
            html +=             '<br />';
            html +=             '<em>';
            html +=                 job.location;
            html +=             '</em>';
            html +=         '</div>';

            let savedJobIds = savedJobs.map(job => job.id);

            if (savedJobIds.indexOf(job.id) === -1) {

                html +=         '<div class="job-right">';
                html +=             '<a href="#" class="save-link" id="' + job.id + '">';
                html +=                 'save';
                html +=             '</a>';
                html +=         '</div>';
            }

            html +=     '</div>';
            html += '</ul>';
        }

        html += '</li>';
    }

    return html;
}

$submit.onclick = showResults;

$location.addEventListener('keyup', e => {

    if (e.key === 'Enter') {
        showResults();
    }
});