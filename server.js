const express = require('express');
const request = require('request');
const app = express();

app.use(express.static(__dirname + '/public'));

app.get('/search/:location', (req, res) => {

	let url = 'https://jobs.github.com/positions.json?location=' + req.params.location;

	request(url, (error, response, body) => {

		res.send(body);
	});
});

app.listen(3000)